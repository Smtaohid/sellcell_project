<?php
use Controller\Database\DatabaseConnection;
include "../../vendor/autoload.php";
$database = new DatabaseConnection();

$admin_table_query = "CREATE TABLE `admins` (`id` Int( 255 ) AUTO_INCREMENT NOT NULL PRIMARY KEY,
 `name` VARCHAR(255) NULL DEFAULT NULL ,
  `email` VARCHAR(255) NULL DEFAULT NULL ,
   `phone` VARCHAR(255) NULL DEFAULT NULL ,
    `password` VARCHAR(255) NULL DEFAULT NULL ,
     `status` INT(255) NULL DEFAULT NULL,
      `create_at` TIMESTAMP  NULL,
      `update_at` TIMESTAMP NULL,
       UNIQUE `email` (`email`),
        UNIQUE `phone` (`phone`)) ENGINE = InnoDB;";
$database->conn->query($admin_table_query);