<?php

use Controller\Database\DatabaseConnection;

include "../../vendor/autoload.php";
$database = new DatabaseConnection();

$categorie_table_query = "CREATE TABLE `categories` (
  `id` INT(255) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NULL,
  `slug` VARCHAR(255) NULL,
  `order_by` INT(255) NULL,
  `status` INT(255) NULL,
  `create_at` TIMESTAMP NULL,
  `update_at` TIMESTAMP NULL,
   PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;";
$database->conn->query($categorie_table_query);
