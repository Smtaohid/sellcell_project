<?php
namespace Controller\Database;


use PDOException;

Class DatabaseConnection{
public $conn;

    public function __construct()
    {
        $server_name = 'localhost';
        $user_name = 'root';
        $password = '';
        $database = 'buysell';

        try {
         $this->conn = new \PDO("mysql:host=$server_name;dbname=$database",$user_name,$password);
            $this->conn->setAttribute(\PDO::ATTR_ERRMODE,\PDO::ERRMODE_EXCEPTION);
//            echo "Connection Successfully";
        }catch (PDOException $e){
         echo "Connection Failed";
        }
    }

    public function __destruct()
    {
        $this->conn = null;
    }
}

