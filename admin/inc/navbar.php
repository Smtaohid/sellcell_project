<?php
use Controller\Auth\Auth;
if (isset($_POST['logout'])){
    $logout = new  Auth();
    $new_logout = $logout->logout();
    if ($new_logout == true){
        header('Location:login.php');
    }
}


?>

<nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
      <!-- Navbar Brand-->
      <a class="navbar-brand ps-3 " href="index.html"><img src="../public/backend/assets/img/logo.webp" alt="logo" class="img-fluid logo"></a>
      <!-- Sidebar Toggle-->
      <button class="btn btn-link btn-sm order-1 order-lg-0 me-4 me-lg-0" id="sidebarToggle" href="#!"><i class="fas fa-bars"></i></button>
      <!-- Navbar-->
      <div class="d-none d-md-inline-block form-inline ms-auto me-0 me-md-3 my-2 my-md-0">
          <ul class="navbar-nav ms-auto ms-md-0 me-3 me-lg-4">
              <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
                  <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                      <li><a class="dropdown-item" href="#!">Settings</a></li>
                      <li><a class="dropdown-item" href="#!">Activity Log</a></li>
                      <li>
                          <hr class="dropdown-divider" />
                      </li>

                      <li>

                      <form method="post">
                         <input type="submit" class="dropdown-item" value="Log Out" name="logout"/>
                      </form>
                      </li>
                  </ul>
              </li>
          </ul>
      </div>
  </nav>
