 <footer class="py-4 bg-light mt-auto">
     <div class="container-fluid px-4">
         <div class="d-flex align-items-center justify-content-between small">
             <div class="text-muted">Copyright &copy; SellCell <?php echo date('Y') ?></div>
             <div>
                 <span class="pe-3 text-muted">Design & Development</span>
                 <a href="#">It Future</a>

             </div>
         </div>
     </div>
 </footer>