<!DOCTYPE html>
<html lang="en">
<?php include('./includs/head.php'); ?>

<body>
    <?php include('./includs/header.php'); ?>
    <?php include('./includs/navbar.php') ?>
    <div class="container-fluid best-price-genarate-banner-sec">
        <div class="row">
            <div class=" col-12 best-price-genarate-banner ">
                <p class="common-color text-center">Over 2 million phones worth over $250 million sold through SellCell since 2008!</p>
                <h2 class="common-color text-center">Cell Phone Trade In Why Use SellCell?</h2>
            </div>
        </div>

    </div>
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-6 my-4 ">
                <div class="card best-price-genarate-content-sec-left">
                    <div class="card-body">
                        <h4 class="mt-4 text-secondary ps-3">By The Numbers</h4>
                        <h2>Over <span>2 million</span> devices worth over <span>$250 million</span> sold through us since <span>2008! </span> </h2>
                    </div>
                </div>

            </div>
            <div class="col-12 col-lg-6 my-4 mx-auto">
                <div class="card best-price-genarate-content-sec-right">
                    <div class="card-body">
                        <h3 class="common-color">Best Price Guarantee</h3>
                        <p>If you are looking to trade in your cell phone there can be a big difference between the highest and lowest prices paid for a cell, and it's never good to find out you've missed out on a better deal (major understatement). Well, you'll be glad to hear that never happens at SellCell.</p>
                        <div class="row mt-4">
                            <div class="col-12 col-lg-8">
                                <h6 class="p common-color">You see, we’re so confident we'll find you the best price for your cell phone or tablet that we're the only site to offer a Best Price Guarantee. If you find a higher price within 24 hours of placing your order, we'll pay you double the difference!! There really is no need to go anywhere else because with SellCell you are always guaranteed the most cash every time!! <a href="#">Read More</a></h6>
                            </div>
                            <div class="col-12 col-lg-4">
                                <img src="public/image/guarantee.png" class="img-fluid img-thumbnail border-0" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>






            <div class="col-12 col-lg-6 my-4 mx-auto">
                <div class="card best-price-genarate-content-sec-right h-auto">
                    <div class="card-body">
                        <h3 class="common-color">We Compare The Whole Market</h3>
                        <div class="row mt-4">
                            <div class="col-12 col-lg-8">
                                <h6 class="p text-secondary">To get the best deal you’d normally have to spend time trawling through lots of different <a class="text-dark" href="#"> cell phone trade in</a> Not anymore! Our unique price comparison engine compares prices from all the leading cell phone trade in sites in seconds. Clever, huh? We compare more phone buyers than any other site and cover the widest range of cell phones and tablets which means you really are guaranteed to always get the most cash for your old electronics</h6>
                            </div>
                            <div class="col-12 col-lg-4">
                                <img src="public/image/compare.png" class="img-fluid img-thumbnail border-0" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12 col-lg-6 my-4 mx-auto">
                <div class="card best-price-genarate-content-sec-right h-auto">
                    <div class="card-body">
                        <h3 class="common-color">We Are No.1!</h3>

                        <div class="row mt-4">
                            <div class="col-12 col-lg-8">
                                <h6 class="p text-secondary">We Are No.1!
                                    SellCell.com was the USA's first cell phone trade in pricecomparison site, and five years later we're still No. 1. It's true. If you’re looking to <a class="text-dark" href="#"> trade in a phone </a> we’re the top site in the US. The cream of the crop. Top of the league. Champions of the… you get the picture.</h6>
                            </div>
                            <div class="col-12 col-lg-4">
                                <img src="public/image/numberone.png" class="img-fluid img-thumbnail border-0" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>





            <div class="col-12 col-lg-6 my-4 mx-auto">
                <div class="card best-price-genarate-content-sec-right h-auto">
                    <div class="card-body">
                        <h3 class="common-color">Trusted</h3>
                        <div class="row mt-4">
                            <div class="col-12 col-lg-8">
                                <h6 class="p text-secondary">More people use us than any other trade in site because they trust our impartial price comparison. Want cold, hard facts? Over 2 million devices worth over $250 million have been sold through us since 2008. Yep, we like big numbers — especially when it comes to getting you the best <a class="text-dark" href="#"> cell phone trade in value</a>. We present unbiased facts and let you decide on the best deal</h6>
                            </div>
                            <div class="col-12 col-lg-4">
                                <img src="public/image/trusted.png" class="img-fluid img-thumbnail border-0" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12 col-lg-6 my-4 mx-auto">
                <div class="card best-price-genarate-content-sec-right h-auto">
                    <div class="card-body">
                        <h3 class="common-color">Saves Time</h3>

                        <div class="row mt-4">
                            <div class="col-12 col-lg-8">
                                <h6 class="p text-secondary">Looking to trade in your phone? Different trade in programs all offer different prices and searching through each one individually to get the best deal takes ages — frankly, there's much better things to do with your time. At SellCell.com our price comparison technology instantly compares prices from all the top cell phone trade in companies to save you time so you can get back to more important stuff</h6>
                            </div>
                            <div class="col-12 col-lg-4">
                                <img src="public/image/savetime.png" class="img-fluid img-thumbnail border-0" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>




            <div class="col-12 col-lg-6 my-4 mx-auto">
                <div class="card best-price-genarate-content-sec-right">
                    <div class="card-body">
                        <h3 class="common-color">SellCell Protection</h3>

                        <div class="row mt-4">
                            <div class="col-12 col-lg-8">
                                <h6 class="p text-secondary">SellCell is the US's No 1 phone trade-in site. We compare over 35 phone & tech buyers to ensure we offer users the best price. SellCell vet all the partners listed in our comparison to make sure that they are reputable businesses with the highest operating standards. Although it is the responsibility of the brand to fulfil your order, if you have any problems with your order SellCell will help. We will offer a second line of defense if you have any frustrations, delays or problems with any of the brands listed on our site. We will liaise with the brand to make sure that they come back to you on any issues promptly and find a resolution to your problem. At SellCell we offer you peace of mind</h6>
                            </div>
                            <div class="col-12 col-lg-4">
                                <img src="public/image/protection.png" class="img-fluid img-thumbnail border-0" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <section>

        <!--testimonials-section-start-from-here-->
        <div class="container section-gap ">
            <div class="row">
                <div class=" text-center py-5 common-color testimonials-section-heading">
                    <h5>What our customers say...</h5>
                </div>
                <div class="col-12 col-lg-3">
                    <div class="card testimonials-section-card  shadow-lg position-relative ">

                        <img src="public/image/26.jpg" class="card-img-top rounded-circle img-fluid testimonials-section-card-img" alt="">
                        <div class="card-body shadow-sm py-5">
                            <span class="fas fa-quote-left h1 common-color"><i class="fas fa-quote-left"></i></span>
                            <blockquote class="d-inline  px-3">SellCell.com found me an awesome deal for my old cell phone.
                                THANKS GUYS!!! <span></span></blockquote>
                            <span class="common-color"><i class="fas fa-quote-right"></i></span>

                        </div>
                    </div>
                    <div class="testimonials-section-address">
                        <address>Thomas Stepien, California</address>
                    </div>

                </div>
                <div class="col-12 col-lg-3">
                    <div class="card testimonials-section-card  shadow-lg position-relative ">

                        <img src="public/image/26.jpg" class="card-img-top rounded-circle img-fluid testimonials-section-card-img" alt="">
                        <div class="card-body shadow-sm py-5">
                            <span class="fas fa-quote-left h1 common-color"><i class="fas fa-quote-left"></i></span>
                            <blockquote class="d-inline  px-3">SellCell.com found me an awesome deal for my old cell phone.
                                THANKS GUYS!!! <span></span></blockquote>
                            <span class="common-color"><i class="fas fa-quote-right"></i></span>

                        </div>
                    </div>
                    <div class="testimonials-section-address">
                        <address>Thomas Stepien, California</address>
                    </div>

                </div>

                <div class="col-12 col-lg-3">
                    <div class="card position-relative shadow-lg  testimonials-section-card ">

                        <img src="public/image/27.jpg" class="card-img-top rounded-circle img-fluid testimonials-section-card-img" alt="">
                        <div class="card-body py-5">
                            <span class="fas fa-quote-left h1 common-color"><i class="fas fa-quote-left"></i></span>
                            <blockquote class="d-inline  px-3">SellCell.com found me an awesome deal for my old cell phone.
                                THANKS GUYS!!! <span></span></blockquote>
                            <span class="common-color"><i class="fas fa-quote-right"></i></span>

                        </div>
                    </div>
                    <div class="testimonials-section-address">
                        <address>Donna Kuper, New York</address>
                    </div>
                </div>
                
                <div class="col-12 col-lg-3">
                    <div class="card position-relative shadow-lg  testimonials-section-card ">

                        <img src="public/image/27.jpg" class="card-img-top rounded-circle img-fluid testimonials-section-card-img" alt="">
                        <div class="card-body py-5">
                            <span class="fas fa-quote-left h1 common-color"><i class="fas fa-quote-left"></i></span>
                            <blockquote class="d-inline  px-3">SellCell.com found me an awesome deal for my old cell phone.
                                THANKS GUYS!!! <span></span></blockquote>
                            <span class="common-color"><i class="fas fa-quote-right"></i></span>

                        </div>
                    </div>
                    <div class="testimonials-section-address">
                        <address>Donna Kuper, New York</address>
                    </div>
                </div>
            </div>
        </div>
        </div>

        <!--    testimonials-logo-section-start-from-here-->
        <div class="container text-center">
            <div class="row">
                <div class="testimonials-logo-section-headind">
                    <h5 class="common-color">As featured in...</h5>
                </div>
                <div class="col-6 col-lg-3 col-md-3">
                    <div class="testimonial-logo">
                        <img src="public/image/forbes.png" class="img-fluid" alt="logo">
                    </div>
                </div>
                <div class="col-6 col-lg-3 col-md-3">
                    <div class="testimonial-logo">
                        <img src="public/image/nbc.png" class="img-fluid " alt="logo">
                    </div>
                </div>
                <div class="col-6 col-lg-3 col-md-3">
                    <div class="testimonial-logo">
                        <img style="height: 129px;width: 167px;" src="public/image/abc_news.png" class="img-fluid pt-5" alt="logo">
                    </div>
                </div>
                <div class="col-6 col-lg-3 col-md-3">
                    <div class="testimonial-logo">
                        <img src="public/image/wsj.png" style=" height: 120px;width: 167px;" class="img-fluid pt-5 " alt="logo">
                    </div>
                </div>
                <div class="col-6 col-lg-3 col-md-3">
                    <div class="testimonial-logo">
                        <img src="public/image/forbes.png" class="img-fluid" alt="logo">
                    </div>
                </div>
                <div class="col-6 col-lg-3 col-md-3">
                    <div class="testimonial-logo">
                        <img src="public/image/nbc.png" class="img-fluid " alt="logo">
                    </div>
                </div>
                <div class="col-6 col-lg-3 col-md-3">
                    <div class="testimonial-logo">
                        <img style="height: 129px;width: 167px;" src="public/image/abc_news.png" class="img-fluid pt-5" alt="logo">
                    </div>
                </div>
                <div class="col-6 col-lg-3 col-md-3">
                    <div class="testimonial-logo">
                        <img src="public/image/wsj.png" style=" height: 120px;width: 167px;" class="img-fluid pt-5 " alt="logo">
                    </div>
                </div>
                <div class="col-6 col-lg-3 col-md-3">
                    <div class="testimonial-logo">
                        <img src="public/image/forbes.png" class="img-fluid" alt="logo">
                    </div>
                </div>
                <div class="col-6 col-lg-3 col-md-3">
                    <div class="testimonial-logo">
                        <img src="public/image/nbc.png" class="img-fluid " alt="logo">
                    </div>
                </div>
                <div class="col-6 col-lg-3 col-md-3">
                    <div class="testimonial-logo">
                        <img style="height: 129px;width: 167px;" src="public/image/abc_news.png" class="img-fluid pt-5" alt="logo">
                    </div>
                </div>
                <div class="col-6 col-lg-3 col-md-3">
                    <div class="testimonial-logo">
                        <img src="public/image/wsj.png" style=" height: 120px;width: 167px;" class="img-fluid pt-5 " alt="logo">
                    </div>
                </div>
            </div>
        </div>
        <div class="press-page text-center my-5">
            <a href="#" class="btn btn-warning text-light fw-bold">Go to Press Page</a>
        </div>
        <!--   testimonials-logo-section-end-here-->
    </section>
    <section>
       <?php include "./includs/footer.php"; ?>
    </section>
</body>
</html>