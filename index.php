<!doctype html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<?php include "./includs/head.php"; ?>

<body>
    <!--topbar-section-start-from-here-->
    <?php include "./includs/header.php"; ?>
    <!--topbar-section-end-here-->


    <!--menubar-section-start-from-here-->
    <?php include "./includs/navbar.php"; ?>
    <!--menubar-section-end-here-->


    <!--banner-section-start-from-here-->
    <div class="container-fluid banner-section">
        <div class="row banner-content">
            <div class="col-lg-7 col-12 text-start banner-content-left ">
                <p class="common-color lead ">SELL PHONE</p>
                <h2 class="">THE MOST CASH FOR YOUR PHONE <span class="common-color">GUARANTEED!</span> <span class="common-color"><i class="fas fa-angle-double-left"></i><i class="fas fa-angle-double-left"></i><i class="fas fa-angle-double-left"></i></span> </h2>
                <img class="img-fluid" src="public/image/banner-image.png" alt="Banner Image" />
                <div class="card shadow border-0 mt-5 banner-card">
                    <div class="card-body bg-white shadow-sm  p-4 p-sm-3 px-lg-5 px-md-4 searchbar ">
                        <div class="input-group p-0 py-sm-4 py-md-2   py-lg-5  mx-lg-auto ">
                            <input type="text" class="form-control px-2 rounded-0" placeholder="Recipient's username" aria-label="Recipient's username" aria-describedby="basic-addon2">
                            <span class="input-group-text btn-warning rounded-0 py-2 d-sm-block" id="basic-addon2">Search..</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 col-12 banner-content-right ">
                <P class="text-light lead ">BUY PHONE</P>

                <h2 class="text-light fw-bold ">PARE DEALS ON REFURBISHED & PRE-OWNED PHONES</h2>
            </div>
        </div>
    </div>
    <!--banner-section-end-here-->


    <!--upgrading-phone-section-start-from-here-->
    <div class="container-fluid section-gap">
        <div class="row">
            <div class="col-12 col-lg-7 mx-lg-auto text-center">
                <h2>Upgrading to the new <span class="common-color">iPhone 12 or Galaxy S21?
                        Sell your old phone here for the most cash!</span></h2>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-10 col-lg-7 mx-auto text-center">
                <img src="public/image/iphone-12-galaxy-s21.webp" alt="Upgrading image" class="img-fluid" type="image/webp" />
                <p class="mt-4 px-lg-5"><strong>TIP: </strong>Values of old models typically drop when new model
                    released -
                    so lock your price in today for up to 30 days!</p>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-7 col-12 mx-lg-auto text-center">
                <a href="#" class="w-100 phone-price-btn pe-lg-3"><button class="btn  rounded-0">GO TO IPHONE
                        PRICE</button></a>
                <a href="#" class="w-100 phone-price-btn pe-lg-3l "><button class="btn rounded-0">GO TO SAMSUNG
                        PRICE</button></a>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-lg-7 mx-auto mt-5 py-2 text-center popular-cellphone">
                <a href="#" class=" lead link-secondary">See Top 100 Most Popular Cell Phone Trade Ins >></a>
            </div>
        </div>
    </div>

    <!--upgrading-phone-section-end-here-->


    <!-- popular-phone-section-start from here-->
    <div class="container section-gap popular-phone-section">
        <div class="row">
            <h2 class="text-center fw-bold text-secondary">MOST POPULAR TRADE INS</h2>
            <?php for ($i = 1; $i <= 26; $i++) { ?>
                <div class="col-12 col-md-6 col-lg-4 my-5">
                    <div class="card border-0 ">
                        <div class="row">

                            <div class="col-6 popular-phone-section-card-img">
                                <a href="./single-priduct.php"><img src="public/image/1-iphone-xr.jpg" class="img-fluid" alt="phone" /></a>
                            </div>
                            <div class="col-6 pt-5 pt-md-0 pt-lg-0">
                                <a href="./single-priduct.php" class="card-title h5  fw-bold text-decoration-none text-dark ">iPhone 12XR</a>
                                <p class="common-color fw-bolder">Top price</p>
                                <h5 class="common-color h4 fw-bolder">315$</h5>
                                <h6 class=" popular-phone-section-compare-price">Compare 34 prices</h6>
                                <a href="./single-priduct.php" class="btn btn-sm common-btn-color text-light fw-bolder">Sell Now</a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
    <!-- popular-phone-section-end-here-->


    <!--    brand-section-start-from-here-->
    <div class="container section-gap">
        <div class="row">
            <h2 class=" text-secondary fw-bold">SEARCH BY BRAND</h2>
            <?php for ($i = 1; $i <= 8; $i++) { ?>
                <div class="col-12 col-md-6 col-lg-3 mt-4 text-center  ">
                    <a href="#">
                        <div class="card border-0 brand-logo">
                            <div class="card-body">
                                <img src="public/image/lg.png" class="card-img" alt="lg-logo" />
                            </div>
                        </div>
                    </a>

                </div>
            <?php } ?>
        </div>
    </div>
    <!--    brand-section-end-here-->


    <!--    sprites-image-brand-section-start-from-here-->
    <div class="container section-gap">
        <div class="row">
            <div class="col-12 col-lg-7 sprites-image-brand text-center mx-auto">
                <img src="public/image/logo.webp" alt="logo" />
                <p class="h5 fw-bold  px-5 mt-3">compares prices from<span class="common-color">35+ leading BuyBack
                        Companies</span>to get you the best deal!</p>
            </div>
        </div>
        <div class="row">
            <?php for ($i = 1; $i <= 8; $i++) { ?>
                <div class="col-6 col-lg-3 col-md-4">
                    <a href="#">
                        <img src="public/image/recycle-logo.png" alt="logo" class="img-fluid border-0" />
                    </a>
                </div>
            <?php } ?>
        </div>
    </div>
    <!--    sprites-image-brand-section-end-here-->


    <!--    sell-your-phone-in-4-simple-steps-section-start-from-here-->
    <div class="container">
        <div class="row">
            <div class="col-12  ">
                <div class="card simple-steps-section">
                    <div class=" border-1 card-header  common-btn-color  py-0 rounded-0 text-light  simple-steps-section-header">
                        <h4>Sell Your Phone in 4 Simple Steps</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-4 ">
                                <div class="row pe-2">
                                    <div class="col-6 text-center">
                                        <img src="public/image/find-your-device.png" alt="search" class="img-fluid" />
                                        <p class="common-color d-block simple-steps-section-text text-danger">1. Find
                                            Your Device</p>
                                    </div>
                                    <div class="col-6 text-center">
                                        <img src="public/image/choose-the-best-deal.png" alt="search" class="img-fluid" />
                                        <p class="common-color simple-steps-section-text common-color">1. Find Your
                                            Device</p>
                                    </div>
                                </div>
                            </div>



                            <div class="col-4">
                                <div class="row ps-2">
                                    <div class="col-6 text-center">
                                        <img src="public/image/post-your-mobile-tablet.png" alt="search" class="img-fluid" />
                                        <p class="common-color simple-steps-section-text text-secondary">1. Find Your
                                            Device</p>
                                    </div>
                                    <div class="col-6 text-center">
                                        <img src="public/image/get-your-cash.png" alt="search" class="img-fluid" />
                                        <p class="common-color d-block simple-steps-section-text text-warning">1. Find
                                            Your Device</p>
                                    </div>
                                </div>
                            </div>






                            <div class="col-4">
                                <div class="card">
                                    <a href="#">
                                        <img class="img-fluid" src="public/image/best-price-guaranteed.png" alt="image" />
                                    </a>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--    sell-your-phone-in-4-simple-steps-section-end-here-->
    <!--testimonials-section-start-from-here-->
    <div class="container section-gap">
        <div class="row">
            <div class=" text-center py-5 common-color testimonials-section-heading">
                <h5>What our customers say...</h5>
            </div>
            <div class="col-12 col-lg-6">
                <div class="card testimonials-section-card  shadow-lg position-relative ">

                    <img src="public/image/26.jpg" class="card-img-top rounded-circle img-fluid testimonials-section-card-img" alt="">
                    <div class="card-body shadow-sm py-5">
                        <span class="fas fa-quote-left h1 common-color"><i class="fas fa-quote-left"></i></span>
                        <blockquote class="d-inline  px-3">SellCell.com found me an awesome deal for my old cell phone.
                            THANKS GUYS!!! <span></span></blockquote>
                        <span class="common-color"><i class="fas fa-quote-right"></i></span>

                    </div>
                </div>
                <div class="testimonials-section-address">
                    <address>Thomas Stepien, California</address>
                </div>

            </div>

            <div class="col-12 col-lg-6">
                <div class="card position-relative shadow-lg  testimonials-section-card ">

                    <img src="public/image/27.jpg" class="card-img-top rounded-circle img-fluid testimonials-section-card-img" alt="">
                    <div class="card-body py-5">
                        <span class="fas fa-quote-left h1 common-color"><i class="fas fa-quote-left"></i></span>
                        <blockquote class="d-inline  px-3">SellCell.com found me an awesome deal for my old cell phone.
                            THANKS GUYS!!! <span></span></blockquote>
                        <span class="common-color"><i class="fas fa-quote-right"></i></span>

                    </div>
                </div>
                <div class="testimonials-section-address">
                    <address>Donna Kuper, New York</address>
                </div>
            </div>
        </div>
    </div>
    </div>

    <!--testimonials-section-end-here-->
    <!--    testimonials-logo-section-start-from-here-->
    <div class="container text-center">
        <div class="row">
            <div class="testimonials-logo-section-headind">
                <h5 class="common-color">As featured in...</h5>
            </div>
            <div class="col-6 col-lg-3 col-md-3">
                <div class="testimonial-logo">
                    <img src="public/image/forbes.png" class="img-fluid" alt="logo">
                </div>
            </div>
            <div class="col-6 col-lg-3 col-md-3">
                <div class="testimonial-logo">
                    <img src="public/image/nbc.png" class="img-fluid " alt="logo">
                </div>
            </div>
            <div class="col-6 col-lg-3 col-md-3">
                <div class="testimonial-logo">
                    <img style="height: 129px;width: 167px;" src="public/image/abc_news.png" class="img-fluid pt-5" alt="logo">
                </div>
            </div>
            <div class="col-6 col-lg-3 col-md-3">
                <div class="testimonial-logo">
                    <img src="public/image/wsj.png" style=" height: 120px;width: 167px;" class="img-fluid pt-5 " alt="logo">
                </div>
            </div>
        </div>
    </div>
    <!--   testimonials-logo-section-end-here-->
    <!-- phone-details-section-start-from-here -->
    <div class="container">
        <div class="row">
            <div class="details-section-heading text-center">
                <h2 class="h3 common-color">Sell Phone</h2>
                <h3 class="common-color  h4 pt-4">SellCell is the Fast and Easy Way to Sell Phones Online</h3>
                <h4 class="theme-color text-secondary h5 pt-2"> "SellCell is like the Kayak of phone selling sites"</h4>
                <em class="p text-secondary">(CentsandOrder)</em>
            </div>
            <div class="col-12 col-lg-6">

                <div class="details-section-disc">
                    <h4 class="common-color ps-2 py-2"> Where Can I Sell My Cell Phone?</h4>
                    <p class="p-2 text-secondary">Fortunately you've come to the right place! SellCell is the best place
                        to sell phones, <a class="text-secondary " href="">tablets</a> and other mobile devices. We
                        compare prices from all the leading cell phone buyers in the US to get you the most cash and
                        also save you time and hassle. If you want to sell a phone or tablet then SellCell is the only
                        site you need!</p>
                </div>
                <div class="details-section-disc">
                    <h4 class="common-color ps-2 py-2"> How Do I Sell My Phone?</h4>
                    <p class="p-2 text-secondary">It's so easy to sell your phone with SellCell. Sell phone for cash
                        today in 4 simple steps:
                    <ol class="text-secondary">

                        <li>
                            Enter the make and model of the cell phone you want to sell in the search box above
                        </li>
                        <li>
                            SellCell instantly compares prices from all the leading cell phone buyback companies to get
                            you the most cash for your old phone. Simply select the best deal

                        </li>
                        <li>
                            Ship your device to your chosen buyback company for free

                        </li>
                        <li>
                            Then simply sit back and wait for your cash!

                        </li>

                    </ol>
                    </p>
                </div>
                <div class="details-section-disc">
                    <div class="details-section-disc">
                        <h4 class="common-color ps-2 py-2"> How Do I Sell My Phone?</h4>
                        <p class="p-2 text-secondary">SellCell.com is the No. 1 price comparison site in the USA for
                            selling used cell phones online. We have helped over 2 million people sell phones & tablets
                            since 2008 so you are in safe hands
                        </p>
                        <p class="p-2 text-secondary">
                            If you're looking to sell cell phone online there are lots of companies out there that will
                            buy your phone but there is a big difference between the highest and lowest prices paid for
                            each model. To get the best deal you would need to spend time trawling through loads of
                            different cell phone buyback companies which is time consuming and it's hard to know which
                            ones you can trust
                        </p>
                        <p class="p-2 text-secondary">
                            Or you can just use SellCell! Our unique valuation engine instantly compares prices from all
                            the top cell phone buyers in the US so it saves you time and you are GUARANTEED to ALWAYS
                            get the most cash for your old cell phone (or we'll pay you double the difference)! Plus,
                            all the cell phone buyers on SellCell have been rigorously vetted and many have worked with
                            us for several years so you know you're dealing with a reputable company. To help further we
                            even transparently show customer reviews for each buyer
                        </p>
                        <p class="p-2 text-secondary">
                            SellCell is completely free and our comparison is totally impartial and unbiased
                        </p>
                        <p class="fw-bold text-secondary">All this makes SellCell the best place to sell used phones,
                            tablets and other mobile devices so why not sell your phone for cash toda</p>
                    </div>
                </div>
                <div class=" mt-3 details-section-disc">
                    <h4 class="common-color ps-2 py-2"> Sell Phone Best Price Guarantee</h4>
                    <p class="p-2 text-secondary">At SellCell we are very confident we'll get you the best price for
                        your old cell phone or mobile device because we compare prices from all the leading cell phone
                        buyers in the US. In fact, we're so confident that if you find a higher price anywhere else,
                        we'll pay you double the difference!!
                    </p>
                    <p class="p-2 text-secondary">There really is no need to go anywhere else to sell a phone because
                        with SellCell you are always guaranteed the most cash every time!!
                    </p>
                </div>
                <div class=" mt-3 details-section-disc">
                    <h4 class="common-color ps-2 py-2"> What is my phone worth? What is my phone value?</h4>
                    <p class="p-2 text-secondary">
                        Want to find out what your phone is worth? Easy. Simply enter the make and model of your device in the search box above. SellCell's unique proprietary technology instantly shows how much your phone is worth. Find out your phone value today and see how much cash you can get for your old phone!
                    </p>
                </div>
                <div class=" mt-3 details-section-disc">
                    <h4 class="common-color ps-2 py-2">Sell Broken Phone? Do You Buy Broken Phones?</h4>
                    <p class="p-2 text-secondary">
                        The answer is YES! Whatever the condition of your old phone you can sell it at SellCell. The cell phone buyers on SellCell buy phones and other mobile devices in any condition so yes you can sell broken phones here. Whether it is just slightly faulty or even if it is completely smashed, cracked or won't turn on there will be a buyer willing to pay for it. Obviously if your device is broken you won't get full value for it, and the more damage the less you will get for it, but it is still worth selling
                    </p>
                    <p class="p-2 text-secondary">
                        How do you <a href="#" class="text-secondary ">sell a broken phone</a> on SellCell? Simple. Search for your device, then under "Condition" select "Broken / Faulty". This filters the results to show who pays the most for your device if it is broken. Some companies specialize in buying broken phones so you might be surprised at how much you can get
                    </p>
                </div>
            </div>



            <div class="col-12 col-lg-6">
                <div class="details-section-disc">
                    <h4 class="common-color ps-2 py-2">Why Wouldn't I Just Sell My Old Phone to My Carrier When I
                        Upgrade?</h4>
                    <p class="p-2 text-secondary">We know how it goes. You've decided to take the plunge and buy that
                        shiny new latest flagship smartphone you've been dreaming about. Hey these things aren't cheap
                        so you're obviously doing the sensible thing of selling your old phone to help pay for the
                        upgrade. You're just going through the process of buying your new phone when your network
                        carrier offers you a trade in price for your old phone. It's so easy, they'll discount your new
                        phone by the trade in price so you have less to pay upfront. It's so tempting and the process is
                        nice and easy. BUT, be warned, this might come at a cost. It's nice and convenient but generally
                        the value you get from the network carriers to sell your old phone isn't the best. You can
                        generally get a lot more cash for your old phone by shopping around and this doesn't have to be
                        a hassle as you can simply use SellCell! SellCell compares prices from all the top cell phone
                        buyers in the US in seconds to get you the best deal. By doing this you can generally get a lot
                        more money for your old phone than the trade in prices offered by the carriers so it is worth
                        doing

                    </p>
                </div>



                <div class="details-section-disc">
                    <h4 class="common-color ps-2 py-2">Who Buys Used Cell Phones?</h4>
                    <p class="p-2 text-secondary">
                        People frequently ask us what happens to their device when they sell a smartphone or tablet
                        through the SellCell platform. The companies featured on SellCell specialize in the buying,
                        selling and recycling of used cell phones, tablets and other mobile devices. There is a big and
                        growing demand globally for used cell phones. The latest handsets continue to get better and
                        better but so does the cost with the prices for new phones increasing all the time. There are a
                        lot of consumers and businesses that prefer to buy refurbished, pre-owned phones for a better
                        deal. The cell phone buyback companies on our site specialize in buying used phones that people
                        no longer want (such as when they upgrade) and they then generally refurbish the devices and
                        sell them on again to customers looking for quality, pre-owned phones. When you trade in your
                        old phone on SellCell the majority of devices will be re-sold to other customers domestically in
                        America but they also get re-sold internationally to other countries all around the world

                    </p>
                </div>

                <div class="details-section-disc">
                    <h4 class="common-color ps-2 py-2">Are There Environmental Benefits to Cell Phone Recycling?</h4>
                    <p class="p-2 text-secondary">
                        It's great getting lots of cash for your old phone but in addition to that when you <a class="text-secondary" href="#">recycle your old cell phone</a> you are helping the
                        environment. E-Waste can be a big problem if electronic goods are not disposed of properly
                        because the materials inside them can be damaging to the environment. When you recycle an old
                        cell phone or other electronic device through SellCell you can be assured it will be processed
                        in an environmentally friendly way. The cell phone buyer you sell it to will either re-sell it
                        on to someone looking for a used phone or if it has no value they will dismantle it and dispose
                        of all the components in an environmentally friendly method. Either way you can rest assured
                        that it will not end up in landfill. In fact, since SellCell was started it has helped millions
                        of phones and electronic devices get recycled in an environmentally friendly way

                    </p>
                    <p class="p-2 text-secondary">It really is Win-Win. You get cash for your old phones and help the
                        environment!</p>
                </div>



                <div class="details-section-disc">
                    <h4 class="common-color ps-2 py-2">Can I Sell Phones in Bulk?</h4>
                    <p class="p-2 text-secondary">
                        Yes. Both consumers and businesses go on to SellCell to sell used phones. If you are a business
                        looking to sell larger batches of phones or other used electronics then one of our account
                        managers will be able to help. Go to our <a class="text-secondary" href=""> Business page</a>
                        for more details on how to<a class="text-secondary" href="">used business phones</a> sell

                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-5 py-3 best-cell-phone-section">
        <div class="container">
            <div class="row">
                <div class="best-cell-phone-section-heading text-center">
                    <h2 class="h3 common-color mb-4">SellCell - the Best Place to Sell Your Phone</h2>
                    <a class="" href="#">Sell Phone</a>
                </div>
            </div>
        </div>
    </div>

    <!-- phone-datails-section-end-here -->
    <?php include "./includs/footer.php"; ?>


    <!--jquery-cdn-->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <!-- Optional JavaScript; choose one of the two! -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->

    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous">
    </script>
    <script src="public/frontend/assete/style/js/main.js"></script>
</body>
</html>