<!doctype html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<?php include "./includs/head.php"; ?>

<body>
    <!--topbar-section-start-from-here-->
    <?php include "./includs/header.php"; ?>
    <!--topbar-section-end-here-->


    <!--menubar-section-start-from-here-->
    <?php include "./includs/navbar.php"; ?>
    <!--menubar-section-end-here-->

    <!-- singl-products-section-start-from-here -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 text-center single-page-add">
                <p>Over 2 million phones worth over $250 million sold through SellCell since 2008!</p>
            </div>
        </div>

    </div>

    <div class="container my-5 display-5">
        <div class="row">
            <div class="col-12">
                <h2>Sell <span class="common-color">Apple iPhone 12</span> </h2>
            </div>
        </div>
        <div class="row mt-2">
            <div class="col-12 col-lg-3 p-0 pt-lg-4">
                <img src="public/image/uploads/products-photo/single-products.jpg" class="img-fluid" alt="">
                <h3 class="common-color"><span class="text-decoration-underline"> Guaranteed</span> most cash for your iPhone XR!</h3>
                <a class="single-products-section-left-link single-page-link" href="#">View Best Price Guarantee </a>
                <hr>
                <p class="lead">shear</p>
                <div class="footer-section-socal-icon text-center">
                    <a href="#" class="single-page-section-socal-icon-link pe-1"><i class="fab fa-twitter-square" aria-hidden="true"></i></a>
                    <a href="#" class="single-page-section-socal-icon-link pe-1"><i class="fab fa-facebook-square" aria-hidden="true"></i></a>
                    <a href="#" class="single-page-section-socal-icon-link"><i class="fas fa-envelope-square" aria-hidden="true"></i></a>
                </div>
                <a href="#" class="card shadow-sm single-page-business-bg-img">
                    <p class="single-page-business-link lead">Selling 25+ devices? Use our <strong>SellCell for Business</strong> service</p>
                </a>

            </div>
            <div class="col-12 col-lg-9">
                <div class="single-page-btn-sec">
                    <h4 class="common-color mt-3">Tell us about your iPhone 12</h4>
                    <span class="h6 lead pe-5 single-page-img-button-cgt">Network</span>
                    <?php for ($i = 29; $i <= 34; $i++) { ?>
                        <a class=" single-page-img-button" href=""><img src="./public/image/<?php echo $i; ?>.png" class="img-fliud single-page-img-button" alt=""></a>
                    <?php } ?>
                </div>
                <div class="single-page-ctg-select-sec">
                    <span class="h6 lead pe-5 single-page-img-button-cgt">Capacity</span>
                    <a href="#" class="single-page-ctg-select-btn text-secondary ms-1  px-1"><input type="radio" name="capacity" id="128GB" value="19" class=" attribute_capacity_search do_ajax" data-attribute="128GB"><label for="128GB" class="p">128GB</label> </a>
                    <a href="#" class="single-page-ctg-select-btn text-secondary ms-1 px-1"><input type="radio" name="capacity" id="256GB" value="19" class=" attribute_capacity_search do_ajax" data-attribute="128GB"><label for="256GB" class="p">256GB</label> </a>
                    <a href="#" class="single-page-ctg-select-btn text-secondary ms-1 px-1"><input type="radio" name="capacity" id="512GB" value="19" class=" attribute_capacity_search do_ajax" data-attribute="128GB"><label for="512GB" class="p">512GB</label></a>
                    <a href="#" class="single-page-ctg-select-btn text-secondary ms-1 px-1"><input type="radio" name="capacity" id="1TB" value="19" class=" attribute_capacity_search do_ajax" data-attribute="128GB"><label for="1TB" class="p">1TB</label> </a>
                </div>
                <div class="single-page-ctg-select-sec">
                    <span class="h6 lead pe-5 single-page-img-button-cgt">Condation</span>
                    <a href="#" class="single-page-ctg-select-btn text-secondary ms-1  px-1">
                        <input type="radio" name="Condation" id="new" data-attribute="likenew">
                        <label for="new" class="p"> LIKE NEW</label>
                    </a>
                    <a href="#" class="single-page-ctg-select-btn text-secondary ms-1 px-1">
                        <input type="radio" name="Condation" id="good" data-attribute="good">
                        <label for="good" class="p">Good</label>
                    </a>
                    <a href="#" class="single-page-ctg-select-btn text-secondary ms-1 px-1">
                        <input type="radio" name="Condation" id="poor" data-attribute="poor">
                        <label for="poor" class="p">POOR</label>
                    </a>
                    <a href="#" class="single-page-ctg-select-btn text-secondary ms-1 px-1">
                        <input type="radio" name="Condation" id="faulty" data-attribute="faulty">
                        <label for="faulty" class="p">FAULTY</label>
                    </a>
                    <a class="lead single-page-link px-2" href="#">Got a iCloud Locked orBlacklisted iPhone XR?</a>
                </div>
                <p class="form-control mt-3 bg-secondary text-light">Apple iPhone XR Trade In Prices</p>
                <div class="recycler-price">

                    <table class=" table table-bordered table-striped table-hover mt-3">
                        <thead>
                            <?php for ($i = 1; $i <= 15; $i++) { ?>
                                <tr class="mt-5">
                                    <th class="text-center col-12 col-lg-4 pb-5 ">
                                        <div class="silgle-page-company-logo">
                                            <img src="public/image/gizmgo.png" class="img-fluid silgle-page-company-logo" alt="logo">
                                        </div>
                                        <div class="single-page-trustpilot-logo">
                                            <img src="public/image/trustpilot-logo.png" class="img-fluid" alt="company logo">
                                            <ul class="rating d-flex list-unstyled justify-content-center">
                                                <?php for ($j = 1; $j <= 5; $j++) { ?>
                                                    <li><span><i class="fas fa-star" aria-hidden="true"></i></span></li>
                                                <?php } ?>

                                            </ul>
                                        </div>
                                    </th>
                                    <th class=" col-lg-4 col-12">
                                        <div class="single-page-compeny-sec-list m-0 pb-3 ">
                                            <ul>
                                                <li class="d-flex flex-column justify-content-center align-content-center align-middle ">
                                                    <p class="text-secondary py-1">FREE SHIPPING? <span><i class="fas fa-check" aria-hidden="true"></i></span></p>
                                                    <p class="text-secondary py-1">FREE RETURNS? <span><i class="fas fa-check" aria-hidden="true"></i></span></p>
                                                    <p class="text-secondary py-1">PAYMENT SPEEDL:<strong class="common-color ms-2 p">NEXT DAY</strong></p>
                                                    <p class="text-secondary py-1">PRICE VALID FOR: 7 DAYS</p>

                                                </li>
                                            </ul>
                                        </div>

                                    </th>
                                    <th class=" col-12 col-lg-4 pb-4 ">
                                        <div class=" text-center single-page-prizing-section">
                                            <h2 class="common-color"> $432. <span>22</span></h2>
                                            <a href="#">Get Paid>></a>
                                        </div>
                                    </th>


                                </tr>
                            <?php } ?>
                        </thead>

                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!-- singl-products-section-end-here -->
    <!-- footer-section-start-from-here -->
    <?php include "./includs/footer.php"; ?>
    <!--jquery-cdn-->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <!-- Optional JavaScript; choose one of the two! -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->

    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous">
    </script>
    <script src="public/frontend/assete/style/js/main.js"></script>
</body>

</html>