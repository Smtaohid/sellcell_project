<!-- best-cell-phone-section-start-from-here -->
<div class="container-fluid mt-5 py-3 best-cell-phone-section">

    <div class="container best-cell-phone-section-footer">
        <div class="row">
            <?php for ($i = 1; $i <= 4; $i++) { ?>
                <div class="col-6 col-md-3 col-lg-3 text-center best-cell-phone-section-img px-3">
                    <img src="public/image/set-1.png" class="img-fluid" alt="">
                </div>
            <?php } ?>
        </div>
        <div class="row">
            <div class="col-12 ">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12 col-md-6 col-lg-4 ">
                                <h5 class="ps-4 common-color h6">SELL YOUR TECH</h5>
                                <a class="best-cell-phone-section-footer-link d-block" href="#"><i class="fas fa-chevron-right"></i> iPhone Trade In</a>
                                <a class="best-cell-phone-section-footer-link d-block" href="#"><i class="fas fa-chevron-right"></i> Samsung Trade In</a>
                                <a class="best-cell-phone-section-footer-link d-block" href="#"><i class="fas fa-chevron-right"></i> Cell Phone Trade In</a>
                                <a class="best-cell-phone-section-footer-link d-block" href="#"><i class="fas fa-chevron-right"></i> Sell iPad</a>
                                <a class="best-cell-phone-section-footer-link d-block" href="#"><i class="fas fa-chevron-right"></i> Sell Tablet</a>
                                <a class="best-cell-phone-section-footer-link d-block" href="#"><i class="fas fa-chevron-right"></i> Sell Smartwatcht</a>
                                <a class="best-cell-phone-section-footer-link d-block" href="#"><i class="fas fa-chevron-right"></i> Search All Brands</a>
                                <a class="best-cell-phone-section-footer-link d-block" href="#"><i class="fas fa-chevron-right"></i> Cell Phone Recycling</a>
                                <a class="best-cell-phone-section-footer-link d-block" href="#"><i class="fas fa-chevron-right"></i> Cell Phone Buyback</a>
                                <a class="best-cell-phone-section-footer-link d-block" href="#"><i class="fas fa-chevron-right"></i> Sell Broken Phone </a>
                                <a class="best-cell-phone-section-footer-link d-block" href="#"><i class="fas fa-chevron-right"></i> How Much Is My Phone Worth?</a>
                                <a class="best-cell-phone-section-footer-link d-block" href="#"><i class="fas fa-chevron-right"></i> Unlock Cell Phone</a>
                                <a class="best-cell-phone-section-footer-link d-block" href="#"><i class="fas fa-chevron-right"></i> Sell Your Phone Canada</a>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4 ">
                                <h5 class="ps-4 common-color h6">About</h5>
                                <a class="best-cell-phone-section-footer-link d-block" href="#"><i class="fas fa-chevron-right"></i> Home</a>
                                <a class="best-cell-phone-section-footer-link d-block" href="#"><i class="fas fa-chevron-right"></i> Why Use SellCell?</a>
                                <a class="best-cell-phone-section-footer-link d-block" href="#"><i class="fas fa-chevron-right"></i> Best Price Guarantee</a>
                                <a class="best-cell-phone-section-footer-link d-block" href="#"><i class="fas fa-chevron-right"></i> Our Partners</a>
                                <a class="best-cell-phone-section-footer-link d-block" href="#"><i class="fas fa-chevron-right"></i> Feature your site on SellCell</a>
                                <a class="best-cell-phone-section-footer-link d-block" href="#"><i class="fas fa-chevron-right"></i> Affiliate Program</a>
                                <a class="best-cell-phone-section-footer-link d-block" href="#"><i class="fas fa-chevron-right"></i> Press</a>
                                <a class="best-cell-phone-section-footer-link d-block" href="#"><i class="fas fa-chevron-right"></i> Blog</a>
                                <a class="best-cell-phone-section-footer-link d-block" href="#"><i class="fas fa-chevron-right"></i>Help Guides </a>
                                <a class="best-cell-phone-section-footer-link d-block" href="#"><i class="fas fa-chevron-right"></i>FAQs</a>
                                <a class="best-cell-phone-section-footer-link d-block" href="#"><i class="fas fa-chevron-right"></i> Help / Contact Us</a>
                                <a class="best-cell-phone-section-footer-link d-block" href="#"><i class="fas fa-chevron-right"></i>Terms</a>
                                <a class="best-cell-phone-section-footer-link d-block" href="#"><i class="fas fa-chevron-right"></i>Privacy</a>
                            </div>
                            <div class="col-12  col-lg-4 text-center ">
                                <h5 class="ps-4 common-color h6">Flow Us</h5>
                                <div class="footer-section-socal-icon text-center">
                                    <a href="#" class="footer-section-socal-icon-link pe-1"><i class="fab fa-twitter-square" aria-hidden="true"></i></a>
                                    <a href="#" class="footer-section-socal-icon-link pe-1"><i class="fab fa-facebook-square" aria-hidden="true"></i></a>
                                    <a href="#" class="footer-section-socal-icon-link"><i class="fab fa-youtube" aria-hidden="true"></i></a>
                                </div>
                                <div class="footer-section-subscribe-from">

                                    <form action="#" method="post">
                                        <div class="input-group mb-3">
                                            <input type="text" class="form-control" placeholder="Enter Your Email">
                                            <input class="btn btn-sm btn-warning text-light" type="submit" value="SUBSCRIBE" id="button-addon2" />
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <div class="row mt-5">
                                <h5 class="ps-4 mb-3 common-color h6">POPULAR SEARCHES </h5>

                                <div class="col-12 col-md-6 col-lg-3 ">
                                    <a class="best-cell-phone-section-footer-link d-block" href="#"><i class="fas fa-chevron-right"></i> Sell iPhone XR </a>
                                    <a class="best-cell-phone-section-footer-link d-block" href="#"><i class="fas fa-chevron-right"></i> Sell iPhone 11</a>
                                    <a class="best-cell-phone-section-footer-link d-block" href="#"><i class="fas fa-chevron-right"></i> Sell iPhone 12 Pro Max</a>
                                    <a class="best-cell-phone-section-footer-link d-block" href="#"><i class="fas fa-chevron-right"></i> Sell iPhone 12 Pro Max</a>

                                </div>
                                <div class="col-12 col-md-6 col-lg-3 ">

                                    <a class="best-cell-phone-section-footer-link d-block" href="#"><i class="fas fa-chevron-right"></i> Sell iPhone X</a>
                                    <a class="best-cell-phone-section-footer-link d-block" href="#"><i class="fas fa-chevron-right"></i> Sell iPhone 8 Plus</a>
                                    <a class="best-cell-phone-section-footer-link d-block" href="#"><i class="fas fa-chevron-right"></i> Sell iPhone XS Max</a>
                                    <a class="best-cell-phone-section-footer-link d-block" href="#"><i class="fas fa-chevron-right"></i> Sell iPhone 11 Pro Max</a>

                                </div>
                                <div class="col-12 col-md-6 col-lg-3 ">

                                    <a class="best-cell-phone-section-footer-link d-block" href="#"><i class="fas fa-chevron-right"></i> Sell iPhone 7S</a>
                                    <a class="best-cell-phone-section-footer-link d-block" href="#"><i class="fas fa-chevron-right"></i> Sell iPhone 7 Plus</a>
                                    <a class="best-cell-phone-section-footer-link d-block" href="#"><i class="fas fa-chevron-right"></i> Sell Galaxy S10+</a>
                                    <a class="best-cell-phone-section-footer-link d-block" href="#"><i class="fas fa-chevron-right"></i> Sell iPhone SE</a>

                                </div>
                                <div class="col-12 col-md-6 col-lg-3 ">

                                    <a class="best-cell-phone-section-footer-link d-block" href="#"><i class="fas fa-chevron-right"></i>Sell iPhone 12</a>
                                    <a class="best-cell-phone-section-footer-link d-block" href="#"><i class="fas fa-chevron-right"></i> Sell iPhone XS</a>
                                    <a class="best-cell-phone-section-footer-link d-block" href="#"><i class="fas fa-chevron-right"></i> Sell Apple Watch</a>
                                    <a class="best-cell-phone-section-footer-link d-block" href="#"><i class="fas fa-chevron-right"></i> Sell Microsoft Surface</a>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>





    </div>

</div>
</div>
<!-- best-cell-phone-section-stahere -->
<footer>
    <div class="container mt-3 mb-5 pb-3">
        <div class="row">
            <div class="col-12 text-center topbar-logo-section"">
                    <img src=" public/frontend/assete/image/logo.webp" alt="">
                <p class="text-secondary px-2 py-3">The Best Place to Sell Your Phone!</p>
            </div>
        </div>
    </div>
</footer>