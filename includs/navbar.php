<!--menubar-section-start-from-here-->
<div class="container-fluid menubar-section">
   <div class="menu">
       <div class="main-menu position-relative  d-flex justify-content-around">
           <span>Sell Your Tech</span>
           <div class="main-menu-item">
               <a class="iphone" href="#">iPhones</a>
               <div class="mega-menu">
                   <div class="row">
                       <div class="col-12 col-md-2 col-lg-3 iphone-menu">
                           <a href="">iPhones</a>
                           <a href="">iPhones</a>
                           <a href="">iPhones</a>
                           <a href="">iPhones</a>

                       </div>
                       <div class="col-12 col-md-2 col-lg-3">
                           <a href="">iPhones</a>
                           <a href="">iPhones</a>
                           <a href="">iPhones</a>
                           <a href="">iPhones</a>

                       </div>
                       <div class="col-12 col-md-2 col-lg-3">
                           <a href="">iPhones</a>
                           <a href="">iPhones</a>
                           <a href="">iPhones</a>
                           <a href="">iPhones</a>

                       </div>
                       <div class="col-12 col-md-2 col-lg-3">
                           <a href="">iPhones</a>
                           <a href="">iPhones</a>
                           <a href="">iPhones</a>
                           <a href="">iPhones</a>

                       </div>
                   </div>
               </div>
           </div>
           <div class="main-menu-item">
               <a href="#">Samsung</a>
           </div>
           <div class="main-menu-item">
               <a href="#">Tablets</a>
           </div>
           <div class="main-menu-item">
               <a href="#"> All Phones</a>
           </div>
           <div class="main-menu-item">
               <a href="#">Best price Guarantee</a>
           </div>
           <div class="main-menu-item">
               <a href="#">Other Tech</a>
           </div>
           <div class="main-menu-item">
               <a href="#">Blog</a>
           </div>
           <div class="main-menu-item">
               <a href="#">Help</a>
           </div>
       </div>
   </div>
</div>
<!--menubar-section-end-here-->