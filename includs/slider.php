
<div class="container-fluid g-0">
    <div class="banner">
        <div class="position-relative">
            <img src="../public/frontend/assete/image/slider-1.jpg" class="img-fluid"  alt="banner">
            <div class="banner-overly-content">
                <h1 class="text-white text-uppercase display-1 text-center">Sell my old phone</h1>
                <h6 class="text-white text-uppercase display-6 text-center mt-5">Over 2 million phones worth over $250 million sold through SellCell since 2008!</h6>
            </div>
        </div>
        <div class="position-relative">
            <img src="../public/frontend/assete/image/slider-2.jpg" class="img-fluid"  alt="banner">
            <div class="banner-overly-content">
                <h1 class="text-white text-uppercase display-1 text-center">Buy old phone</h1>
                <h6 class="text-white text-uppercase display-6 text-center mt-5">Over 2 million phones worth over $250 million sold through SellCell since 2008!</h6>
            </div>
        </div>
        <div class="position-relative">
            <img src="../public/frontend/assete/image/slider-3.jpg" class="img-fluid" alt="banner">
            <div class="banner-overly-content">
                <h1 class="text-white text-uppercase display-1 text-center">Sell my old phone</h1>
                <h6 class="text-white text-uppercase display-6 text-center mt-5">Over 2 million phones worth over $250 million sold through SellCell since 2008!</h6>
            </div>
        </div>
    </div>
</div>


