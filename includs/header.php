 <!--topbar-section-start-from-here-->
 <div class="container-fluid topbar ">
     <div class="container container-md-fluid w-sm-100">
         <div class="row py-3">
             <div class=" col-6 col-lg-4 col-md-4  topbar-logo-section">
                 <img src="public/image/logo.webp" class="img-fluid" type="image/webp" />
             </div>
             <div class="col-6  d-lg-none d-md-none text-end responsive-menu">
                 <span class="h1"><i class="fas fa-bars "></i></span>
             </div>

             <div class="    col-lg-4  col-md-4  justify-content-center  d-flex  topbar-center-section">
                 <img src="./public/image/checked.svg" class="img-fluid topbar-icon" type="image/webp" alt="" />
                 <h4>Over 2 million phones worth over $250 million sold through SellCell since 2008!</h4>
             </div>
             <div class=" col-12 col-lg-4 col-md-4 text-end button-area ">
                 <a class="w-50" href="#"><button class=" solid-button">Buy</button></a>
                 <a class="w-50" href=""><button class=" outline-button">Sell</button></a>
             </div>

         </div>
     </div>
 </div>
 <!--topbar-section-end-here-->